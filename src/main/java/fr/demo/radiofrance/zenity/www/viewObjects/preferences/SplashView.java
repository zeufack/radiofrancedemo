package fr.demo.radiofrance.zenity.www.viewObjects.preferences;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class SplashView extends View {

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/onboarding_next_button")
    private MobileElement nextButton;

    public void hintNextButton() {
        int hintCount = 0;
        loadingWait.until(visibilityOf(this.nextButton));
            while (hintCount < 2) {
                super.wait.until(visibilityOf(this.nextButton)).click();
                hintCount++;
            }
    }
}
