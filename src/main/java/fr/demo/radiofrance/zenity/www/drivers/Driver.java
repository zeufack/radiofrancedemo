package fr.demo.radiofrance.zenity.www.drivers;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public interface Driver {

    // this function will be redefine for local an remote appium server
    AppiumDriver<MobileElement> getDriver();

    // release resource by closing driver
    void closeDriver();

    /**
     * 
     * @return
     */
    default DesiredCapabilities androidDesiredCapabilities(){
        return  new DesiredCapabilities();
     }
}
