package fr.demo.radiofrance.zenity.www.enums;

public enum Platform {
    ANDROID,
    ANDROID_EMULATOR,
    IOS,
    IOS_EMULATOR
}
