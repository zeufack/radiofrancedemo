package fr.demo.radiofrance.zenity.www.drivers.implementation;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import fr.demo.radiofrance.zenity.www.drivers.Driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import fr.demo.radiofrance.zenity.www.enums.Location;
import fr.demo.radiofrance.zenity.www.enums.Platform;

import static  fr.demo.radiofrance.zenity.www.context.Properties.FILE_PROPERTIES;
import static  fr.demo.radiofrance.zenity.www.context.Properties.CAPABILITIES_CONFIG;;


public class LocalDriver implements Driver{

    private AppiumDriver<MobileElement> driver;

    public LocalDriver(Location location, Platform platform, String device) {
        switch (platform) {
            case ANDROID : case ANDROID_EMULATOR:
                 this.driver = createAndroidDriver(location, platform, device);
            default:
                break;
        }
    }


    @Override
    public AppiumDriver<MobileElement> getDriver() {
        return this.driver;
    }

    @Override
    public void closeDriver() {
        this.driver.close();        
    }

    
    private AppiumDriver<MobileElement> createAndroidDriver(Location location, Platform platform, String device) {
        DesiredCapabilities capabilities = Config.getCapabilities(location, platform, device);
        capabilities.setCapability(MobileCapabilityType.APP, FILE_PROPERTIES.androidAppPath);
        capabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD, true);
        capabilities.setCapability(AndroidMobileCapabilityType.RESET_KEYBOARD, true);
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
        capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
       
        return new AndroidDriver<>(Config.getAppiumUrl(), capabilities);
    }

    private static class Config {
        private static String APPIUM_HOST = FILE_PROPERTIES.appiumServerHost;
        private static String APPIUM_PORT = FILE_PROPERTIES.appiumServerPort;

        private static DesiredCapabilities getCapabilities(Location location, Platform platform, String device) {

            return CAPABILITIES_CONFIG.getDesiredCapabilities(location, platform, device);
        }

        private static URL getAppiumUrl() {
            try {
                return new URL("http://"+APPIUM_HOST+":"+APPIUM_PORT+"/wd/hub");
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
}
