package fr.demo.radiofrance.zenity.www.viewObjects.auth.registration;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class RegistrationView extends View {

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_register_email_edittext")
    private MobileElement registerEmail;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_register_password_edittext")
    private MobileElement registerPassword;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_loading_button_button")
    private MobileElement loadingButton;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_register_login_textview")
    private MobileElement registerLogin;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/textinput_error")
    private MobileElement inputError;

    public boolean inputErrorIsVisible() {
        return waitUntil(visibilityOf(this.inputError));
    }

    public void setRegisterEmail(String registerEmail) {
        super.wait.until(visibilityOf(this.registerEmail)).sendKeys(registerEmail);
    }

    public void setRegisterPassword(String registerPassword) {
        super.wait.until(visibilityOf(this.registerPassword)).sendKeys(registerPassword);
    }

    public void clickLoadingButton() {
        super.loadingWait.until(elementToBeClickable(this.loadingButton)).click();
    }

    public void clickRegisterLogin() {
        super.loadingWait.until(elementToBeClickable(this.registerLogin)).click();
    }
}
