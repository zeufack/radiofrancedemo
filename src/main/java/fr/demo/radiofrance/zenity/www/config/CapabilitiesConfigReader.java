package fr.demo.radiofrance.zenity.www.config;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.demo.radiofrance.zenity.www.enums.Location;
import fr.demo.radiofrance.zenity.www.enums.Platform;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.remote.DesiredCapabilities;

public enum CapabilitiesConfigReader {
    INSTANCE;

    private static final Logger LOG = LoggerFactory.getLogger(CapabilitiesConfigReader.class);

    private static final String CONFIG_FILE_PATH = "config/capabilities.conf.json";

    private static final JsonNode node;

    static {
        try (JsonParser parser = new ObjectMapper().getFactory().createParser(new FileReader(CONFIG_FILE_PATH))) {
            node = parser.readValueAsTree();
        } catch (IOException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static CapabilitiesConfigReader getInstance() {
        return INSTANCE;
    }

    public DesiredCapabilities getDesiredCapabilities(Location location, Platform platform, String device) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        
        TypeReference typeReference = new TypeReference<Map<String, String>>() {
        };

        // convert json informations to key value map
        Map<String, String> capabilitiesMap = (Map<String, String>) new ObjectMapper()
                .convertValue(this.getCapabilitiesJson(location, platform, device), typeReference);
        capabilitiesMap.forEach(capabilities::setCapability);
        return capabilities;

    }

    /**
     * retrive capabilities information from json file
     * @param location
     * @param platform
     * @param device
     * @return 
     */
    private JsonNode getCapabilitiesJson(Location location, Platform platform, String device) {
        System.out.println(node.get(location.name().toLowerCase()).get(platform.name().toLowerCase()).get(device));
        
        return node.get(location.name().toLowerCase()).get(platform.name().toLowerCase());
    }

    
}
