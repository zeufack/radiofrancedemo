package fr.demo.radiofrance.zenity.www.viewObjects.auth.recovery;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class ForgottenPasswordView extends View {

    

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_forgot_password_email_edittext")
    private MobileElement forgotPasswordEmail;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_loading_button_button")
    private MobileElement loadingButton;

    public void setForgotPassordEmail(String forgotPasswordEmail) {
        super.wait.until(visibilityOf(this.forgotPasswordEmail)).sendKeys(forgotPasswordEmail);

    }

    public void clickLoadingButton() {
        super.loadingWait.until(elementToBeClickable(this.loadingButton)).click();
    }

    

}
