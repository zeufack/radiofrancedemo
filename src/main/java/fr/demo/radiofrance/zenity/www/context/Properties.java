package fr.demo.radiofrance.zenity.www.context;

import fr.demo.radiofrance.zenity.www.config.CapabilitiesConfigReader;
import fr.demo.radiofrance.zenity.www.config.FilePropertiesReader;
import fr.demo.radiofrance.zenity.www.config.SystemPropertiesReader;

public interface Properties {
    FilePropertiesReader FILE_PROPERTIES = FilePropertiesReader.getInstance();
    SystemPropertiesReader SYSTEM_PROPERTIES = SystemPropertiesReader.getInstance();
    CapabilitiesConfigReader CAPABILITIES_CONFIG = CapabilitiesConfigReader.getInstance();

}
