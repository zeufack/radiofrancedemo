package fr.demo.radiofrance.zenity.www.drivers;

import fr.demo.radiofrance.zenity.www.drivers.implementation.LocalDriver;
import fr.demo.radiofrance.zenity.www.enums.Location;
import fr.demo.radiofrance.zenity.www.enums.Platform;

/*
 * use Factory desing pattern ass we will like our framework to 
 * use different type of driver for diferent location
 */
public final class DriverFactory {
    public static Driver getDriver(Location location, Platform platform, String device) {
        switch (location) {
            case LOCAL:
                return new LocalDriver(location, platform, device);
            default:
                return null;
        }
    }
}
