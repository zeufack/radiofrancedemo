package fr.demo.radiofrance.zenity.www.viewObjects.auth.account;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class AccountView extends View {
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.RelativeLayout")
    private MobileElement logOutButton;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout")
    private MobileElement changePasswordButton;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.RelativeLayout")
    private MobileElement deleteAccount;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/action_bar_root")
    private MobileElement logoutFrame;

    @AndroidFindBy(id = "android:id/button1")
    private MobileElement logOutConfirmButton;

    public void clickLogOutButton() {
        if (waitUntil(visibilityOf(this.logOutButton))) {
            this.logOutButton.click();
        }
    }

    public void clickChangePasswordButton() {
        if (waitUntil(visibilityOf(this.changePasswordButton))) {
            this.changePasswordButton.click();
        }
    }

    public void clickDeleteAccount() {
        if (waitUntil(visibilityOf(this.deleteAccount))) {
            this.deleteAccount.click();
        }
    }

    public void clickLogOutConfirm() {
        loadingWait.until(visibilityOf(this.logOutConfirmButton));
        // super.switchFrame(this.logoutFrame);
        super.wait.until(visibilityOf(this.logOutConfirmButton)).click();
    }

}
