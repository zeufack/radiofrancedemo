package fr.demo.radiofrance.zenity.www.viewObjects.auth.recovery;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class ForgottPasswordSuccess extends View {

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_forgot_password_success_title")
    private MobileElement accountForgotSuccess;

    public boolean isAccountForgotSuccess() {
        return waitUntil(visibilityOf(this.accountForgotSuccess));
    }
    
}
