package fr.demo.radiofrance.zenity.www.enums;

public enum Environment {
    PRODUCTION,
    PREPROD,
    SINT,
    FEATURE
}
