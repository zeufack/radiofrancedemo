package fr.demo.radiofrance.zenity.www.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public enum FilePropertiesReader {

    INSTANCE;

    private static final Logger LOG = LoggerFactory.getLogger(FilePropertiesReader.class);
    private static final String PROPERTIES_FILENAME = "application.properties";
    private static final String PROPERTIES_LOCATION = "config/" + PROPERTIES_FILENAME;

    private Properties properties;

    public final String androidAppPath;
    public final String nodeJsPath;
    public final String appiumJsPath;
    public final String appiumServerHost;
    public final String appiumServerPort;
    public final String proxy;

    public static FilePropertiesReader getInstance() {
        return INSTANCE;
    }


    FilePropertiesReader() {
        try (BufferedReader reader = new BufferedReader(new FileReader(PROPERTIES_LOCATION))) {
            this.properties = new Properties();
            this.properties.load(reader);

            this.androidAppPath   = readProperty("androidAppPath");
            this.nodeJsPath       = readProperty("nodeJsPath");
            this.appiumJsPath     = readProperty("appiumJsPath");
            this.appiumServerHost = readProperty("appiumServerHost");
            this.appiumServerPort = readProperty("appiumServerPort");
            this.proxy            = readProperty("proxy");

        } catch (IOException e) {
            throw new IllegalArgumentException("application.properties not found in resources folder");
        }
    }

    private String readProperty(String key) {
        String property = this.properties.getProperty(key);
        if (property == null) {
            LOG.warn("{} value is missing in config.properties", key);
        }
        return property;
    }
    
}
