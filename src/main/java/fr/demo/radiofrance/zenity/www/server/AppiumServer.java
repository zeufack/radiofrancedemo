package fr.demo.radiofrance.zenity.www.server;

import java.io.IOException;
import java.net.ServerSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

import static  fr.demo.radiofrance.zenity.www.context.Properties.FILE_PROPERTIES;

/*
 * as this class is service class we make it final to avoid instantiation
 */
public final class AppiumServer {
    // instantiate logger
    private static final Logger LOG = LoggerFactory.getLogger(AppiumServer.class);

    private static String APPIUM_HOST = FILE_PROPERTIES.appiumServerHost;
    private static String APPIUM_PORT = FILE_PROPERTIES.appiumServerPort;

    // appium local service make it possible to start and stop appium server
    // programmatically
    private static AppiumDriverLocalService appiumDriverLocalService;

    /*
     * this function try to start appium server
     */
    public static void start() {
        // appium service builder help us to define appium server config option
        AppiumServiceBuilder appiumServiceBuilder = new AppiumServiceBuilder()
                .withIPAddress(APPIUM_HOST)
                .usingPort(Integer.parseInt(APPIUM_PORT))
                .withArgument(GeneralServerFlag.LOG_LEVEL, "info")
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withArgument(GeneralServerFlag.RELAXED_SECURITY);
        appiumDriverLocalService =AppiumDriverLocalService.buildService(appiumServiceBuilder);
        appiumDriverLocalService.start();
        LOG.info("server started successfully");

    }

    public static void stop() {
        try {
            appiumDriverLocalService.stop();
            LOG.info("server stoped successfully");
        } catch (Exception e) {
            LOG.error("Stopping server enconter problem");
        }
    }

    /**
     * 
     * @return a boolean reprensenting server running status
     */
    public static boolean isRunning() {
        try {
            // ping the appium server port
            new ServerSocket(Integer.parseInt(APPIUM_PORT)).close();
        } catch (IOException e) {
            return true;
        }
        return false;
    }

}
