package fr.demo.radiofrance.zenity.www.viewObjects.preferences;

import java.util.List;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class SelectDepartmentView extends View {


    @AndroidFindBy(id = "com.radiofrance.radio.francebleu.android:id/department_title")
    private MobileElement viewTitle;

    @AndroidFindBy(id = "com.radiofrance.radio.francebleu.android:id/search_edit")
    private MobileElement departmentSearchEdit;

    @AndroidFindBy(id = "com.radiofrance.radio.francebleu.android:id/department_list")
    private List<MobileElement> departmentList;


    public boolean getDepartmentViewTitle() {
        loadingWait.until(visibilityOf(this.viewTitle));
        return this.viewTitle.getText().equals("Sélectionnez votre département");
    }

    
}
