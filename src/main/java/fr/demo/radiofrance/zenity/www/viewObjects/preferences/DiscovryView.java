package fr.demo.radiofrance.zenity.www.viewObjects.preferences;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class DiscovryView extends View{
    
    @AndroidFindBy(id = "com.radiofrance.radio.francebleu.android:id/boarding_discovery_button")
    private MobileElement discoveryButton;

    public void hintDiscovryButton(){
        super.wait.until(visibilityOf(this.discoveryButton)).click();
    }
}
