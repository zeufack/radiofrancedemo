package fr.demo.radiofrance.zenity.www.viewObjects.auth.login;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class LoginView extends View {

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_login_email_edittext")
    private MobileElement loginEmail;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_login_password_edittext")
    private MobileElement loginPassword;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_loading_button_button")
    private MobileElement loadingButton;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_login_forgot_password")
    private MobileElement loginForgotPassword;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/rf_account_login_registration_button")
    private MobileElement registrationButton;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/textinput_error")
    private MobileElement inputError;

    public void setLoginEmail(String loginEmail) {
        super.wait.until(visibilityOf(this.loginEmail)).sendKeys(loginEmail);
    }

    public void setLoginPassword(String loginPassword) {
        super.wait.until(visibilityOf(this.loginPassword)).sendKeys(loginPassword);
    }

    public void clickLoadingButton() {
        super.loadingWait.until(elementToBeClickable(this.loadingButton)).click();
    }

    public void clickLoginForgotPassword() {
        super.loadingWait.until(elementToBeClickable(this.loginForgotPassword)).click();
    }

    public void clickRegistrationButton() {
        if (waitUntil(visibilityOf(this.registrationButton))) {
             super.loadingWait.until(elementToBeClickable(this.registrationButton)).click();
        }
    }

    public boolean inputErrorIsVisible() {
        return waitUntil(visibilityOf(this.inputError));
    }

}
