package fr.demo.radiofrance.zenity.www.viewObjects;

import java.util.function.Function;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import fr.demo.radiofrance.zenity.www.manager.DriverManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class View {
    protected final AppiumDriver<MobileElement> driver;

    @AndroidFindBy(accessibility = "Navigate up")
    private MobileElement navigateUp;

    protected WebDriverWait wait;
    protected WebDriverWait loadingWait;
    protected WebDriverWait longWait;
    protected WebDriverWait shortWait;

    public View() {
        this.driver         = DriverManager.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
        this.wait           = new WebDriverWait(this.driver, 5);
        this.loadingWait    = new WebDriverWait(this.driver, 30);
        this.longWait       = new WebDriverWait(this.driver, 60);
        this.shortWait      = new WebDriverWait(this.driver, 1);
    }

    public void navigateBack() {
        this.loadingWait.until(elementToBeClickable(this.navigateUp)).click();
    }

    public AndroidDriver<MobileElement> getAndoidDriver() {
        return (AndroidDriver<MobileElement>) driver;
    }

    protected <V> boolean shortWaitUntil(Function<? super WebDriver, V> isTrue) {
        try {
            shortWait.until(isTrue);
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    protected <V> boolean waitUntil(Function<? super WebDriver, V> isTrue) {
        try {
            wait.until(isTrue);
            return true;
        } catch (TimeoutException e) {
            return false;
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

    protected <V> boolean loadingWaitUntil(Function<? super WebDriver, V> isTrue) {
        try {
            loadingWait.until(isTrue);
            return true;
        } catch (TimeoutException e) {
            return false;
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

    protected <V> boolean longWaitUntil(Function<? super WebDriver, V> isTrue) {
        try {
            this.longWait.until(isTrue);
            return true;
        } catch (TimeoutException e) {
            return false;
        } catch (StaleElementReferenceException e) {
            return false;
        }
    }

    protected void set(MobileElement elem, String text, String placeHolder) {
        this.wait.until(visibilityOf(elem)).clear();
        elem.sendKeys(text);
        this.wait.until(driver -> elem.getText().equalsIgnoreCase(text));
    }

    protected void set(MobileElement elem, String text) {
        this.set(elem, text, null);
    }

    protected void click(MobileElement elem) {
        this.wait.until(elementToBeClickable(elem)).click();
    }

    protected void longPress(MobileElement elem) {
        this.wait.until(elementToBeClickable(elem));
        new Actions(this.driver)
                .clickAndHold(elem)
                .perform();
    }

    protected void switchFrame(MobileElement element) {
        this.wait.until(elementToBeClickable(element));
        this.driver.switchTo().frame(element);
    }
}
