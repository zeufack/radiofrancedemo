package fr.demo.radiofrance.zenity.www.viewObjects.preferences;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class PreferencesView extends View {
    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/button_disagree")
    private MobileElement buttonDisagree;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/button_agree")
    private MobileElement buttonAgree;


    public void clikOnAgreeButton() {
        loadingWait.until(visibilityOf(this.buttonAgree));
            if(waitUntil(visibilityOf(this.buttonAgree))){
            super.wait.until(visibilityOf(this.buttonAgree)).click();}
        
    }
}
