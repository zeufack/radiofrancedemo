package fr.demo.radiofrance.zenity.www.config;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;

import fr.demo.radiofrance.zenity.www.enums.Environment;
import fr.demo.radiofrance.zenity.www.enums.Location;
import fr.demo.radiofrance.zenity.www.enums.Platform;

public enum SystemPropertiesReader {
    INSTANCE;

    // private static final Logger LOG = LoggerFactory.getLogger(SystemPropertiesReader.class);

    private static final String PLATFORM_KEY = "platform";
    private static final String LOCATION_KEY = "location";
    private static final String HASHED_APP_ID_KEY = "hashed-app-id";
    private static final String ENVIRONMENT_KEY = "env";
    private static final String DEVICE_KEY = "device";

    public final Platform platform;
    public final Location location;
    public final String hashedAppId;
    public final Environment environment;
    public final String device;

    public static SystemPropertiesReader getInstance() {
        return INSTANCE;
    }

    SystemPropertiesReader() {
        this.platform = Platform.valueOf(System.getProperty(PLATFORM_KEY, "android_emulator").toUpperCase());
        this.location = Location.valueOf(System.getProperty(LOCATION_KEY, "local").toUpperCase());
        this.hashedAppId = System.getProperty(HASHED_APP_ID_KEY, "").toUpperCase();
        this.environment = Environment.valueOf(System.getProperty(ENVIRONMENT_KEY, "Production").toUpperCase());
        this.device=System.getProperty(DEVICE_KEY, "samsung_s9");
    }

}
