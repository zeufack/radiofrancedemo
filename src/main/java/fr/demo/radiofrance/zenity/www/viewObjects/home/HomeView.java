package fr.demo.radiofrance.zenity.www.viewObjects.home;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;


public class HomeView extends View {
    @AndroidFindBy(accessibility = "More options")
    private MobileElement moreOptions;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/navigation_podcasts")
    private MobileElement navigationPodcasts;

    @AndroidFindBy(accessibility = "com.radiofrance.radio.radiofrance.android:id/navigation_radio")
    private MobileElement navigationRadio;

    @AndroidFindBy(accessibility = "com.radiofrance.radio.radiofrance.android:id/navigation_browse")
    private MobileElement navigationBrowse;

    @AndroidFindBy(accessibility = "com.radiofrance.radio.radiofrance.android:id/navigation_library")
    private MobileElement navigationLibrary;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.LinearLayout")
    private MobileElement loginNavButton;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/main_coordinatorlayout")
    private MobileElement mainLayot;

    public void clickOnMoreOption() {
        if (waitUntil(visibilityOf(this.moreOptions))) {
            super.wait.until(visibilityOf(this.moreOptions)).click();
        }
    }

    public void clinkOnNavigationPodcasts() {
        super.wait.until(visibilityOf(this.navigationPodcasts)).click();

    }

    public void clinkOnNavigationRadio() {
        super.wait.until(visibilityOf(this.navigationRadio)).click();

    }

    public void clinkOnNavigationBrowse() {
        super.wait.until(visibilityOf(this.navigationBrowse)).click();

    }

    public void clinkOnNavigationLibrary() {
        super.wait.until(visibilityOf(this.navigationLibrary)).click();

    }

    public void clikLoginNavButton(){
        if (waitUntil(visibilityOf(this.loginNavButton))) {
            this.loginNavButton.click();
        }
    }

    public boolean isHomeViews(){
        return waitUntil(visibilityOf(this.mainLayot));
    }

}
