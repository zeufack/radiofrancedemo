package fr.demo.radiofrance.zenity.www.manager;

import fr.demo.radiofrance.zenity.www.drivers.Driver;
import fr.demo.radiofrance.zenity.www.drivers.DriverFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import static  fr.demo.radiofrance.zenity.www.context.Properties.*;




public final class DriverManager {
    private static final DriverManager INSTANCE = new DriverManager();

    private static ThreadLocal<Driver> driver = new ThreadLocal<>();

    private DriverManager() {}

    public static DriverManager getInstance() {
        return INSTANCE;
    }

    public static AppiumDriver<MobileElement> getDriver() {
        return driver.get().getDriver();
    }

    public static void setDriver(String device) {
        driver.set(DriverFactory.getDriver(SYSTEM_PROPERTIES.location, SYSTEM_PROPERTIES.platform, device));
    }
}
