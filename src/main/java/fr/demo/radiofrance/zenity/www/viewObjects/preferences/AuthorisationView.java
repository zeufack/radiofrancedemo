package fr.demo.radiofrance.zenity.www.viewObjects.preferences;

import fr.demo.radiofrance.zenity.www.viewObjects.View;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class AuthorisationView extends View {

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/onboarding_finish_button")
    private MobileElement onboardingFinishButton;

    @AndroidFindBy(id = "com.radiofrance.radio.radiofrance.android:id/onboarding_nothanks_button")
    private MobileElement onboardingNothanksButton;

    public void clickOnboardingFinishButton() {
        if (waitUntil(visibilityOf(this.onboardingFinishButton))) {
            super.wait.until(visibilityOf(this.onboardingFinishButton)).click();
        }
    }

    public void clickOnboardingNothanksButton() {
        loadingWait.until(visibilityOf(this.onboardingNothanksButton));
        super.wait.until(visibilityOf(this.onboardingNothanksButton)).click();

    }
}
