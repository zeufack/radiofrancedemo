Feature: Login

    # Background: Select Preferences:
    #     Given  I hint accept all button
    #     And I hint next button three time
    #     And I am redirect to set authorisation


    Scenario Outline: : login with email and password violating policies
        Given I navigate to login page
        When I log in with "<email>" and "<password>"
        Then The I should not be able to get in
        Examples:
            | email          | password        |
            | userEmail      | invalidPassword |
            | user@email.com | 111111111111111 |
            | user@email.fr  | 1               |


    Scenario Outline: Successfuly log in
        Given I am on the login page
        When I login with "<email>" and "<password>"
        Then I shoud be invalidPassword
        Examples:
            | email                     | password    |
            | ivarsonthorgaut@gmail.com | 000UserPass |


    Scenario: log out 
        Given I am on home page
        When I try to log out 
        Then My session should be close

    
    
