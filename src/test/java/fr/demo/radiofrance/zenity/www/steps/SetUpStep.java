package fr.demo.radiofrance.zenity.www.steps;


import fr.demo.radiofrance.zenity.www.viewObjects.preferences.AuthorisationView;
import fr.demo.radiofrance.zenity.www.viewObjects.preferences.PreferencesView;
import fr.demo.radiofrance.zenity.www.viewObjects.preferences.SplashView;
import io.cucumber.java8.En;


public class SetUpStep implements En {

    /*
     *
     */
    public SetUpStep(PreferencesView preferencesView, SplashView splashView, AuthorisationView authorisationView) {
        
        Given("I hint accept all button", ()->{
             preferencesView.clikOnAgreeButton();
        });

        And("I hint next button three time", ()->{
           splashView.hintNextButton();
        });

        And("I am redirect to set authorisation", ()->{
           authorisationView.clickOnboardingNothanksButton();
        });
    }
    
}
