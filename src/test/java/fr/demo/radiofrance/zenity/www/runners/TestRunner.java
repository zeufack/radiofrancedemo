package fr.demo.radiofrance.zenity.www.runners;

import org.testng.annotations.*;

import fr.demo.radiofrance.zenity.www.manager.DriverManager;
import fr.demo.radiofrance.zenity.www.server.AppiumServer;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

import static fr.demo.radiofrance.zenity.www.context.Properties.SYSTEM_PROPERTIES;;

@CucumberOptions(plugin = {
        "pretty",
        "html:target/reports/html/htmlreport",
        "json:target/reports/jsonreports/index.json",
        "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"
}, features = { "src/test/resources/features" }, glue = { "fr.demo.radiofrance.zenity.www.steps" })
public class TestRunner extends AbstractTestNGCucumberTests {
    // private PreferencesView preferencesView = new PreferencesView();
    // private AuthorisationView authorisationView = new AuthorisationView();
    // private SplashView splashView = new SplashView();


    /**
     * start local appium server and create a session before running our test
     * 
     * @param device
     */
    @BeforeSuite
    public void beforeSuite(@Optional("") String device) {
        /*
         * start appium local server if not running
         */
        if (!AppiumServer.isRunning() && this.isLocal()) {
            AppiumServer.start();
        }

        /*
         * create a new session
         * for connection to appium server with appium java clien
         */
        DriverManager.setDriver(device);
        /*
         * Accept cookies and applications preference before stating test suite
         */
        // this.acceptCookies();

    }

    /*
     * Run after all our feature
     */
    @AfterClass
    public void afterSuite() {

        // close appium java client session
        DriverManager.getDriver().quit();

        // close appium local server if still running
        if (AppiumServer.isRunning() && this.isLocal()) {
            AppiumServer.stop();
        }

    }

    // read environement from système properties file
    private boolean isLocal() {
        return SYSTEM_PROPERTIES.location.name().toLowerCase().equals("local");
    }

    // private void acceptCookies() {
    //     this.preferencesView.clikOnAgreeButton();
    //     this.splashView.hintNextButton();
    //     this.authorisationView.clickOnboardingNothanksButton();
    // }

}
