package fr.demo.radiofrance.zenity.www.steps;

import org.testng.Assert;

import fr.demo.radiofrance.zenity.www.viewObjects.auth.account.AccountView;
import fr.demo.radiofrance.zenity.www.viewObjects.auth.login.LoginView;
import fr.demo.radiofrance.zenity.www.viewObjects.home.HomeView;
import io.cucumber.java8.En;

public class LoginStep implements En {
    public LoginStep(HomeView homeView, LoginView loginView, AccountView accountView) {
        Given("I navigate to login page", () -> {
            homeView.clickOnMoreOption();
            homeView.clikLoginNavButton();
        });
        When("I log in with {string} and {string}", (String email, String password) -> {
            loginView.setLoginEmail(email);
            loginView.setLoginPassword(password);
            loginView.clickLoadingButton();
        });


        Then("The I should not be able to get in", () -> {
            Assert.assertTrue(loginView.inputErrorIsVisible());
        });

        /**
         * Log out
         */
        Given("I am on home page", () -> {
            homeView.clickOnMoreOption();
            homeView.clikLoginNavButton();
        });

        When("I try to log out", () -> {
            accountView.clickLogOutButton();
            accountView.clickLogOutConfirm();
        });

        Then("My session should be close", () -> {
            Assert.assertTrue(homeView.isHomeViews());
        });
        
    }
}
