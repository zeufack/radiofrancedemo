package fr.demo.radiofrance.zenity.www.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// import org.testng.annotations.BeforeSuite;
// import org.testng.annotations.BeforeTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

// import fr.demo.radiofrance.zenity.www.manager.DriverManager;
import fr.demo.radiofrance.zenity.www.viewObjects.preferences.AuthorisationView;
import fr.demo.radiofrance.zenity.www.viewObjects.preferences.PreferencesView;
import fr.demo.radiofrance.zenity.www.viewObjects.preferences.SplashView;
// import io.appium.java_client.AppiumDriver;
// import io.appium.java_client.MobileElement;
import io.cucumber.java8.En;

public class Hooks implements En {
    private final Logger LOG = LoggerFactory.getLogger(Hooks.class);
    private static boolean dunit = false;

    public Hooks(AuthorisationView authorisationView, PreferencesView preferencesView, SplashView splashView) {
        /**
         * accept cookies an naviguate to home page
         */
        Before(0, () -> {
            if (!Hooks.dunit) {
                preferencesView.clikOnAgreeButton();
                splashView.hintNextButton();
                authorisationView.clickOnboardingNothanksButton();
                Hooks.dunit = true;
            }
        });
    }
}
