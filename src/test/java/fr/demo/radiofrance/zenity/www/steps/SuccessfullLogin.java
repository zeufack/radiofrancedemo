package fr.demo.radiofrance.zenity.www.steps;

import org.testng.Assert;

import fr.demo.radiofrance.zenity.www.viewObjects.auth.login.LoginView;
import fr.demo.radiofrance.zenity.www.viewObjects.home.HomeView;
import io.cucumber.java8.En;

public class SuccessfullLogin implements En {
    public SuccessfullLogin(HomeView homeView, LoginView loginView){
        Given("I am on the login page", () -> {
            homeView.clickOnMoreOption();
            homeView.clikLoginNavButton();
        });
        When("I login with {string} and {string}", (String email, String password) -> {
            loginView.setLoginEmail(email);
            loginView.setLoginPassword(password);
            loginView.clickLoadingButton();
        });
        Then("I shoud be invalidPassword", () -> {
            Assert.assertTrue(homeView.isHomeViews());
        });
    }
}
