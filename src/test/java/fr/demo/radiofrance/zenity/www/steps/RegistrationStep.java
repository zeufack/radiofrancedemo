package fr.demo.radiofrance.zenity.www.steps;

import org.testng.Assert;

import fr.demo.radiofrance.zenity.www.viewObjects.auth.login.LoginView;
import fr.demo.radiofrance.zenity.www.viewObjects.auth.registration.RegistrationView;
import fr.demo.radiofrance.zenity.www.viewObjects.home.HomeView;
import io.cucumber.java8.En;

public class RegistrationStep implements En {
    public RegistrationStep(HomeView homeView, LoginView loginView, RegistrationView registrationView) {
        Given("I navigate to registration page", ()->{
            homeView.clickOnMoreOption();
            homeView.clikLoginNavButton();
            loginView.clickRegistrationButton();
        });
        When("I create my account with {string} and {string}", (String email, String password) -> {
            registrationView.setRegisterEmail(email);
            registrationView.setRegisterPassword(password);
            registrationView.clickLoadingButton();
        });

        Then("The I should not be able to create an account", () -> {
            // Todo pass boolean instead of String
            Assert.assertTrue(registrationView.inputErrorIsVisible());
        });
    }
}
