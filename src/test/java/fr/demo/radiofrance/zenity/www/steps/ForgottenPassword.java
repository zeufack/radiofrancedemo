package fr.demo.radiofrance.zenity.www.steps;

import io.cucumber.java8.En;

public class ForgottenPassword implements En {
    public ForgottenPassword(){
        Given("I am on forgotten password view", () -> {
            // Write code here that turns the phrase above into concrete actions
            // throw new io.cucumber.java8.PendingException();
        });
        When("I enter {string}", (String string) -> {
            // Write code here that turns the phrase above into concrete actions
            // throw new io.cucumber.java8.PendingException();
        });
        Then("I should have receive an email", () -> {
            // Write code here that turns the phrase above into concrete actions
            // throw new io.cucumber.java8.PendingException();
        });
    }
}
