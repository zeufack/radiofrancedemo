Feature: Registration

  # Background: Select Preferences:
  #   Given  I hint accept all button
  #   And I hint next button three time
  #   And I am redirect to set authorisation

  Scenario Outline: : create a new account with email and password violating policies
    Given I navigate to registration page
    When I create my account with "<email>" and "<password>"
    Then The I should not be able to create an account
    Examples:
      | email          |  password       |   
      | userEmail      | invalidPassword |
      | user@email.com | 111111111111111 |
      | user@email.fr  | 1               |

  # Scenario: create a new account with good password
  #   Given I navigate to registration page
  #   When I create my account with "<email>" and "<password>"
  #   Then The I should not be able to create an account
  #   Examples:
  #     | email          |  password       |   
  #     | user@email.com | 111userpa111111 |


